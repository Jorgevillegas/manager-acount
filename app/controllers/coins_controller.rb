class CoinsController < ApplicationController
	before_action :set_coin, except: [ :create, :new, :index]

	def index
		@coins = Coin.all

		render json: @coins
	end

	def show
		render json: @coin
	end

	def new
		@coin = Coin.new
	end

	def create
		@coin = Coin.create(coin_params)
	end

	private
	# Use callbacks to share common setup or constraints between actions.
	def set_coin
		@coin = Coin.find(params[:id]) if params[:id]
	end

  # Only allow a list of trusted parameters through.
	def coin_params
		params.require(:coin).permit(:name, :symbol)
	end
end