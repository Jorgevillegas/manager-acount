Rails.application.routes.draw do

  resources :acounts
  resources :vendors
  resources :coins
  resources :movements
  resources :coin_values
  
end
