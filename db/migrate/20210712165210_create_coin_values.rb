class CreateCoinValues < ActiveRecord::Migration[6.1]
  def change
    create_table :coin_values do |t|
      t.references :coin, null: false, foreign_key: true
      t.float :amount

      t.timestamps
    end
  end
end
