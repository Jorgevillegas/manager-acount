class Movement < ApplicationRecord
  belongs_to :acount
  belongs_to :vendor
  before_create :create_balance
  after_save :update_acount
  after_destroy :fix_acount
  before_update :fix_acount

  def fix_acount

    value = amount_was
    value = -amount_was unless status 

      acount.update!(mount: ( acount.mount - value))
  end

  def update_acount
    value = amount
    value = -amount unless status 

      acount.update!(mount: ( acount.mount + value))

  end 
  def create_balance
    if status
      self.balance = acount.mount + amount 
    else
      self.balance = acount.mount - amount
    end

  end

end
