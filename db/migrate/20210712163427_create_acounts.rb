class CreateAcounts < ActiveRecord::Migration[6.1]
  def change
    create_table :acounts do |t|
      t.string :detail
      t.references :coin, null: false, foreign_key: true
      t.float :mount

      t.timestamps
    end
  end
end
