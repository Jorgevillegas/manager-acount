class CoinSerializer < ActiveModel::Serializer
  attributes :name, :symbol
end
