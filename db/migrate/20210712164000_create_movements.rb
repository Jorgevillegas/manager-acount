class CreateMovements < ActiveRecord::Migration[6.1]
  def change
    create_table :movements do |t|
      t.references :acount, null: false, foreign_key: true
      t.references :vendor, null: false, foreign_key: true
      t.boolean :status
      t.date :date
      t.string :detail
      t.float :amount
      t.float :balance

      t.timestamps
    end
  end
end
