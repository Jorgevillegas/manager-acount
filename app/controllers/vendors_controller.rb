class VendorsController < ApplicationController
	before_action :set_params, only: [:show, :edit, :update, :destroy]
	def index
		@vendors = Vendor.all
		render json: @vendors
	end

	def show
		render json: @vendor
	end

	def new
	end

	def create
		@vendor = Vendor.create(vendor_params)
	end

	private

	def set_params
		@vendor = Vendor.find(params[:id])
	end

	def vendor_params
		params.require(:vendor).permit(:name, :details)
	end
end
