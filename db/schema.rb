# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_07_12_165210) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "acounts", force: :cascade do |t|
    t.string "detail"
    t.bigint "coin_id", null: false
    t.float "mount"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["coin_id"], name: "index_acounts_on_coin_id"
  end

  create_table "coin_values", force: :cascade do |t|
    t.bigint "coin_id", null: false
    t.float "amount"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["coin_id"], name: "index_coin_values_on_coin_id"
  end

  create_table "coins", force: :cascade do |t|
    t.string "name"
    t.string "symbol"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "movements", force: :cascade do |t|
    t.bigint "acount_id", null: false
    t.bigint "vendor_id", null: false
    t.boolean "status"
    t.date "date"
    t.string "detail"
    t.float "amount"
    t.float "balance"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["acount_id"], name: "index_movements_on_acount_id"
    t.index ["vendor_id"], name: "index_movements_on_vendor_id"
  end

  create_table "vendors", force: :cascade do |t|
    t.string "name"
    t.string "details"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  add_foreign_key "acounts", "coins"
  add_foreign_key "coin_values", "coins"
  add_foreign_key "movements", "acounts"
  add_foreign_key "movements", "vendors"
end
