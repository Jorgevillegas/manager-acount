class AcountsController < ApplicationController
	before_action :set_acount, except: [:create, :new, :index]

	def index
		@acounts = Acount.all
		render json: @acounts
	end

	def show
		render json: @acount
	end

	def new
		@acount = Acount.new
	end

	def create
		@acount = Acount.create(acount_params)
	end

	private
	# Use callbacks to share common setup or constraints between actions.
	def set_acount
		@acount = Acount.find(params[:id]) if params[:id]
	end

  # Only allow a list of trusted parameters through.
	def acount_params
		params.require(:acount).permit(:name, :symbol)
	end
end
