class MovementsController <  ApplicationController
	before_action :set_movement, only: [:show, :edit, :update, :destroy]

	def index
		@movements = Movement.all
		render json: @movements
	end

	def show
		render jso: @movement
	end
		
	def new
		@movement = Movement.new
	end

	def create
		@movement = Movement.create(movement_params)
	end
	
	private
	def set_movement
		@movement = Movement.find_by(params[:id])
	end

	def movement_params
		params.require(:movement).permit(:acount_id, :vendor_id, :status, :date, :detail, :amount)
	end
end